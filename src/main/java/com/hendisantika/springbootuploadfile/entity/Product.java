package com.hendisantika.springbootuploadfile.entity;

import lombok.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-file
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/08/18
 * Time: 09.25
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    private String id;
    private String name;
    private double price;
    private String photo;
}
