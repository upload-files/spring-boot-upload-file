package com.hendisantika.springbootuploadfile.controller;

import com.hendisantika.springbootuploadfile.entity.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-file
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/08/18
 * Time: 09.26
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "product")
public class ProductController implements ServletContextAware {

    private static final Logger logger = LogManager.getLogger(ProductController.class);

    //    private static String UPLOADED_FOLDER = "/tmp/uploads/";
    @Value("${uploaded.folder}")
    private String UPLOADED_FOLDER;

    private ServletContext servletContext;


    @Value("${server.servlet.context-path}")
    private String contextPath;

    @GetMapping
    public String index(Model m) {
        m.addAttribute("product", new Product());
        return "product/add";
    }

    @PostMapping("save")
    public String save(@ModelAttribute("product") Product product, @RequestParam(value = "file") MultipartFile file,
                       Model model) {
        String fileName = saveImage(file);
        product.setPhoto(fileName);
        model.addAttribute("product", product);
        logger.info("Data Product : {}", product);

//        return "redirect:/product/success";
        return "redirect:/success";
    }

    private String saveImage(MultipartFile multipartFile) {
        try {
            byte[] bytes = multipartFile.getBytes();
//            Path path = Paths.get(servletContext.getRealPath("/tmp/uploads/" + multipartFile.getOriginalFilename()));
            Path path = Paths.get(UPLOADED_FOLDER + multipartFile.getOriginalFilename());
            Files.write(path, bytes);
            return multipartFile.getOriginalFilename();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

}